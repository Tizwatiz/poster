//
//  SignupText.swift
//  Poster
//
//  Created by Kevin Hammond on 4/19/20.
//  Copyright © 2020 Tizwatiz. All rights reserved.
//

import SwiftUI

struct SignupText: View {
    var body: some View {
        HStack{
            Text("Don't have an account?").font(.footnote).foregroundColor(.gray)
            Text("Sign up").foregroundColor(.black)
        }
    }
}
