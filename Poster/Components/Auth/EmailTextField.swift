//
//  EmailTextField.swift
//  Poster
//
//  Created by Kevin Hammond on 4/19/20.
//  Copyright © 2020 Tizwatiz. All rights reserved.
//

import SwiftUI

struct EmailTextField: View {
    
    @Binding var email: String
    
    var body: some View {
        HStack{
            Image(systemName: "envelope.fill").foregroundColor(Color(red: 0, green: 0, blue: 0, opacity: 0.3))
            TextField("Email", text: $email)
        }.modifier(TextFieldModifier())
    }
}
