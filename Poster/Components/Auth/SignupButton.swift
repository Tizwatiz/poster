//
//  SignupButton.swift
//  Poster
//
//  Created by Kevin Hammond on 4/25/20.
//  Copyright © 2020 Tizwatiz. All rights reserved.
//

import SwiftUI

struct SignupButton: View {
    
    var action: () -> Void
    var label: String
    
    var body: some View {
        Button(action: action) {
            HStack {
                Spacer()
                Text(label).fontWeight(.bold).foregroundColor(Color.white)
                Spacer()
            }
        }.modifier(SignInButtonModifier())
    }
}
