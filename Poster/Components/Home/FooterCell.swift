//
//  FooterCell.swift
//  Poster
//
//  Created by Kevin Hammond on 8/1/20.
//  Copyright © 2020 Tizwatiz. All rights reserved.
//

import SwiftUI

struct FooterCell: View {
    var body: some View {
        VStack(alignment: .leading, spacing: 8) {
            HStack {
                Image(systemName: "heart.fill")
                NavigationLink(destination: CommentView()) {
                    Image(systemName: "bubble.left").renderingMode(.original)
                }
                
                Image(systemName: "paperplane")
                Spacer()
                Image(systemName: "bookmark.fill")
            }
            HStack {
                Text("David").font(.subheadline).bold()
                Text("Black and White").font(.subheadline)
            }
            NavigationLink(destination: CommentView()) {
            Text("View all comments").font(.caption).foregroundColor(.gray)
            }
            HStack {
                Image("photo1").resizable().clipShape(Circle()).frame(width: 25, height: 25)
                Text("Add a comment...").font(.caption).foregroundColor(.gray)
                Spacer()
                Text("♥️")
                Text("👏")
                Image(systemName: "plus.circle").foregroundColor(.gray)
            }
            
            Text("1 day ago").font(.caption).foregroundColor(.gray)
        }.padding(.leading, 15).padding(.trailing, 15)
    }
}

struct FooterCell_Previews: PreviewProvider {
    static var previews: some View {
        FooterCell()
    }
}
