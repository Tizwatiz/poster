//
//  CommentRow.swift
//  Poster
//
//  Created by Kevin Hammond on 8/2/20.
//  Copyright © 2020 Tizwatiz. All rights reserved.
//

import SwiftUI

struct CommentRow: View {
    var body: some View {
        HStack {
            Image("photo1").resizable().clipShape(Circle()).frame(width: 35, height: 35)
            VStack(alignment: .leading) {
                Text("David").font(.subheadline).bold()
                Text("location").font(.caption)
            }
            Spacer()
            Text("1 day ago").font(.caption).foregroundColor(.gray)
        }.padding(.leading, 15).padding(.trailing, 15)
    }
}

struct CommentRow_Previews: PreviewProvider {
    static var previews: some View {
        CommentRow()
    }
}
