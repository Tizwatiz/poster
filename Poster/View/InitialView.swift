//
//  InitialView.swift
//  Poster
//
//  Created by Kevin Hammond on 6/15/20.
//  Copyright © 2020 Tizwatiz. All rights reserved.
//

import SwiftUI

struct InitialView: View {
    
    @EnvironmentObject var session: SessionStore
    
    func listen() {
        session.listenAuthenticationState()
        //session.logout()
        //session.isLoggedIn = false
    }
    
    var body: some View {
        Group {
            if session.isLoggedIn {
               MainView()
            } else {
               SigninView()
            }
        }.onAppear(perform: listen)
    }
}
