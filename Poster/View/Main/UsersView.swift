//
//  UsersView.swift
//  Poster
//
//  Created by Kevin Hammond on 8/4/20.
//  Copyright © 2020 Tizwatiz. All rights reserved.
//

import SwiftUI

struct UsersView: View {
    
    @State var searchText: String = ""
    
    func searchTextdidChange() {
        print("searchTextDidChange")
        print(searchText)
        // Find Users
    }
    
    var body: some View {
            VStack {
                SearchBar(text: $searchText, onSearchButtonChanged: searchTextdidChange)
                List {
                    ForEach(0..<10) { _ in
                        HStack {
                            Image("photo1").resizable().clipShape(Circle()).frame(width: 50, height: 50)
                            VStack(alignment: .leading, spacing: 5) {
                                Text("David").font(.headline).bold()
                                Text("iOS Developer").font(.subheadline)
                            }
                        }.padding(10)
                    }
                }
            }.navigationBarTitle(Text("Search"), displayMode: .inline)
    }
}

struct UsersView_Previews: PreviewProvider {
    static var previews: some View {
        UsersView()
    }
}
