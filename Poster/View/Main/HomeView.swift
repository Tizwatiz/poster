//
//  HomeView.swift
//  Poster
//
//  Created by Kevin Hammond on 6/21/20.
//  Copyright © 2020 Tizwatiz. All rights reserved.
//

import SwiftUI

struct HomeView: View {
    var body: some View {
        NavigationView {
            ScrollView {
                Story()
                ForEach(1..<20) { _ in
                    HeaderCell()
                    FooterCell()
                }
            }.navigationBarTitle(Text("Poster"), displayMode: .inline)
            
        }
    }
}

struct HomeView_Previews: PreviewProvider {
    static var previews: some View {
        HomeView()
    }
}
