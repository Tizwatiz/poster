//
//  CameraView.swift
//  Poster
//
//  Created by Kevin Hammond on 8/3/20.
//  Copyright © 2020 Tizwatiz. All rights reserved.
//

import SwiftUI

struct CameraView: View {
    var body: some View {
        NavigationView {
            VStack {
                HStack(alignment: .top) {
                    Image("photo").resizable().scaledToFill().frame(width: 60, height: 60).clipped()
                    TextField("Write a caption...", text: .constant("")).padding(.top, 5)
                }.padding()
                Spacer()
            }.navigationBarTitle(Text("Camera"), displayMode: .inline).navigationBarItems(trailing: Button(action: {}) {
                //Text("Share").font(.headline)
                Image(systemName: "arrow.turn.up.right").imageScale(Image.Scale.large)
                
            }).foregroundColor(.black)
        }
    }
}

struct CameraView_Previews: PreviewProvider {
    static var previews: some View {
        CameraView()
    }
}
