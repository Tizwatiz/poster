//
//  User.swift
//  Poster
//
//  Created by Kevin Hammond on 6/7/20.
//  Copyright © 2020 Tizwatiz. All rights reserved.
//

import Foundation



struct User: Encodable, Decodable {
    var uid: String
    var email: String
    var profileImageUrl: String
    var username: String
    var bio: String
    var keywords: [String]
    
//    var dict: [String: Any] {
//        return ["uid": uid, "email": email, "profileImageUrl": profileImageUrl, "username": username, "bio": bio, "keywords": keywords]
//    }
    
}
